// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs"),
	url = require('url'),
	mime = require('mime'),
	path = require('path');

var rooms = new Array();
rooms[0] = ["Lobby"];
var privaterooms = new Array();
privaterooms[0] = ["Mystery Room"];
var privateroomspasswords = ["sam"];
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
	
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	rooms_for_client = new Array();
	for (var i = 0; i < rooms.length; i++) {
		rooms_for_client[i] = rooms[i][0];
	}
	privaterooms_for_client = new Array();
	for (var i = 0; i < privaterooms.length; i++) {
		privaterooms_for_client[i] = privaterooms[i][0];
	}
	
	socket.emit("allRooms", {allRooms:rooms_for_client, allPrivateRooms:privaterooms_for_client});
 
	socket.on('new_user', function(data) {
		console.log("new user: "+data["usr"]);
		rooms[0].push(data["usr"]);
		console.log("rooms[0]="+rooms[0]);
		io.sockets.emit("new_user_toclient",{room:"Lobby", users:rooms[0]})
	});
	
	socket.on('change_rooms_toserver_public', function(data) {
		//add user to user list for room he's switching into & delete user from user list for oldroom
		var index;
		for (var i = 0; i < rooms.length; i++) {
			if (rooms[i][0] == data["room"])
				index = i;
		}
		var q;
		rooms[index].push(data["user"]);
		for (var i = 0; i < rooms.length; i++) {
			if (rooms[i][0] == data["oldroom"]) {
				q = i;
				console.log(data["oldroom"] + " users:  " + rooms[q]);
				for (var j = 0; j < rooms[i].length; j++) {
					if (rooms[i][j] == data["user"]) {
						rooms[i].splice(j,1);
					}
				}
			}
		}
		//console.log(data["user"] + " has left " + data["oldroom"] + " for " + data["room"] + ". Users in this room are now: " + rooms[index]);
		console.log(data["oldroom"] + " users:  " + rooms[q]);
		io.sockets.emit("new_user_toclient",{room:data["room"], users:rooms[index], usersold:rooms[q], oldroom:data["oldroom"]})
	});

	socket.on('change_rooms_toserver_private', function(data) {
		var index;
		for (var i = 0; i < privaterooms.length; i++) {
			if (privaterooms[i][0] == data["room"])
				index = i;
		}
		var q;
		privaterooms[index].push(data["username"]);
		for (var i = 0; i < privaterooms.length; i++) {
			if (privaterooms[i][0] == data["oldroom"]) {
				q = i;
				for (var j = 0; j < privaterooms[i].length; j++) {
					if (privaterooms[i][j] == data["user"]) {
						privaterooms[i].splice(j,1);
					}
				}
			}
		}
		io.sockets.emit("new_user_toclient",{room:data["room"], users:privaterooms[index], usersold:privaterooms[q], oldroom:data["oldroom"]})
	});
 
	socket.on('message_to_server', function(data) {
		console.log("message: "+data["message"]); // log it to the Node.JS output
		io.sockets.emit("message_to_client",{room:data["room"], message:data["message"]})
	});
	
	socket.on('create_room', function(data) {
		rooms.push(data["room"]);
		rooms[rooms.indexOf(data["room"])] = [data["room"],data["user"]];
		var index;
		for (var i = 0; i < rooms.length; i++) {
			console.log(rooms[i]);
			if (rooms[i][0] == data["oldroom"]) {
				index = i;
				for (var j = 0; j < rooms[index].length; j++) {
					if (rooms[index][j] == data["user"]) {
						rooms[index].splice(j,1);
					}
				}
			}
		}
		console.log("room added: " + rooms[rooms.length-1]);
		console.log("old room: " + rooms[index]);
		io.sockets.emit("newRoom",{room:data["room"],oldroom:data["oldroom"], usersold:rooms[index]})
	});
	
	socket.on('create_privateroom', function(data) {
		privaterooms.push(data["room"]);
		privaterooms[privaterooms.indexOf(data["room"])] = [data["room"]];
		privateroomspasswords.push(data["password"]);
		var index;
		for (var i = 0; i < privaterooms.length; i++) {
			if (privaterooms[i][0] == data["oldroom"]) {
				index = i;
				for (var j = 0; j < privaterooms[i].length; j++) {
					if (privaterooms[i][j] == data["user"]) {
						privaterooms[i].splice(j,1);
					}
				}
			}
		}
		console.log("private room added: " + privaterooms[privaterooms.length-1]);
		io.sockets.emit("newPrivateRoom",{room:data["room"], oldroom:data["oldroom"], usersold:privaterooms[index]})
	});
	
	socket.on('enter_privateroom', function(data) {
		var index;
		for (var i = 0; i < privaterooms.length; i++) {
			if (privaterooms[i][0] == data["room"])
				index = i;
		}
		if (privateroomspasswords[index] == data["password"]) {
			//send back private room name to specific socket
			io.sockets.emit("enterPrivateRoom",{room:data["room"]})
			for (var i = 0; i < privaterooms.length; i++) {
			if (privaterooms[i][0] == data["oldroom"]) {
				for (var j = 0; j < privaterooms[i].length; j++) {
					if (privaterooms[i][j] == data["user"]) {
						privaterooms[i].splice(j,1);
					}
				}
			}
		}
		}
		else io.sockets.emit("enterPrivateRoom",{room:"ERRORPASSWORDINCORRECT"});
	});
	
	
});